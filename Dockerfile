FROM node:16

#RUN npm install pm2 -g

WORKDIR /home/discord_message_bot

COPY package*.json /home/discord_message_bot/

RUN npm i

COPY . /home/discord_message_bot
EXPOSE 7882

CMD ["node", "index.js"]
